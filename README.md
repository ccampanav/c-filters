Released at: April 2018

Description: Load an image and apply some filters to it.
Filters availables: Flip horizontally, Flip vertically, Grayscale, Binarized, Inverted colors, 8 colors, Edge Detection & Segmentation
