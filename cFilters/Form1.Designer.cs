﻿namespace cFilters
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pbOriginal = new System.Windows.Forms.PictureBox();
            this.pbFilter = new System.Windows.Forms.PictureBox();
            this.lbOriginal = new System.Windows.Forms.Label();
            this.lbFilter = new System.Windows.Forms.Label();
            this.btOpen = new System.Windows.Forms.Button();
            this.ofdOriginal = new System.Windows.Forms.OpenFileDialog();
            this.btnSave = new System.Windows.Forms.Button();
            this.cbFilters = new System.Windows.Forms.ComboBox();
            this.btProcess = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbOriginal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFilter)).BeginInit();
            this.SuspendLayout();
            // 
            // pbOriginal
            // 
            this.pbOriginal.Cursor = System.Windows.Forms.Cursors.Cross;
            this.pbOriginal.Location = new System.Drawing.Point(10, 60);
            this.pbOriginal.Name = "pbOriginal";
            this.pbOriginal.Size = new System.Drawing.Size(400, 300);
            this.pbOriginal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbOriginal.TabIndex = 0;
            this.pbOriginal.TabStop = false;
            // 
            // pbFilter
            // 
            this.pbFilter.Cursor = System.Windows.Forms.Cursors.Cross;
            this.pbFilter.Location = new System.Drawing.Point(420, 60);
            this.pbFilter.Name = "pbFilter";
            this.pbFilter.Size = new System.Drawing.Size(400, 300);
            this.pbFilter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbFilter.TabIndex = 1;
            this.pbFilter.TabStop = false;
            // 
            // lbOriginal
            // 
            this.lbOriginal.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOriginal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(240)))));
            this.lbOriginal.Location = new System.Drawing.Point(10, 10);
            this.lbOriginal.Name = "lbOriginal";
            this.lbOriginal.Size = new System.Drawing.Size(400, 44);
            this.lbOriginal.TabIndex = 2;
            this.lbOriginal.Text = "ORIGINAL";
            this.lbOriginal.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbFilter
            // 
            this.lbFilter.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFilter.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(240)))));
            this.lbFilter.Location = new System.Drawing.Point(420, 10);
            this.lbFilter.Name = "lbFilter";
            this.lbFilter.Size = new System.Drawing.Size(400, 44);
            this.lbFilter.TabIndex = 3;
            this.lbFilter.Text = "FILTRADA";
            this.lbFilter.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btOpen
            // 
            this.btOpen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btOpen.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btOpen.Location = new System.Drawing.Point(10, 366);
            this.btOpen.Name = "btOpen";
            this.btOpen.Size = new System.Drawing.Size(68, 28);
            this.btOpen.TabIndex = 4;
            this.btOpen.Text = "Abrir";
            this.btOpen.UseVisualStyleBackColor = true;
            this.btOpen.Click += new System.EventHandler(this.btOpen_Click);
            // 
            // ofdOriginal
            // 
            this.ofdOriginal.Title = "Selecciona una imagen";
            // 
            // btnSave
            // 
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(752, 366);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(68, 28);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Guardar";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cbFilters
            // 
            this.cbFilters.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFilters.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbFilters.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFilters.ForeColor = System.Drawing.Color.DimGray;
            this.cbFilters.FormattingEnabled = true;
            this.cbFilters.Items.AddRange(new object[] {
            "Sin filtros",
            "Invertir en horizontal",
            "Invertir en vertical",
            "Escala de grises",
            "Binarizada",
            "Colores invertidos",
            "8 colores",
            "Detección de bordes",
            "Segmentación"});
            this.cbFilters.Location = new System.Drawing.Point(291, 367);
            this.cbFilters.Name = "cbFilters";
            this.cbFilters.Size = new System.Drawing.Size(240, 28);
            this.cbFilters.TabIndex = 6;
            // 
            // btProcess
            // 
            this.btProcess.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btProcess.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btProcess.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btProcess.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(240)))));
            this.btProcess.Location = new System.Drawing.Point(538, 366);
            this.btProcess.Name = "btProcess";
            this.btProcess.Size = new System.Drawing.Size(100, 28);
            this.btProcess.TabIndex = 7;
            this.btProcess.Text = "Procesar";
            this.btProcess.UseVisualStyleBackColor = true;
            this.btProcess.Click += new System.EventHandler(this.btProcess_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(829, 411);
            this.Controls.Add(this.btProcess);
            this.Controls.Add(this.cbFilters);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btOpen);
            this.Controls.Add(this.lbFilter);
            this.Controls.Add(this.lbOriginal);
            this.Controls.Add(this.pbFilter);
            this.Controls.Add(this.pbOriginal);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DimGray;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximumSize = new System.Drawing.Size(845, 450);
            this.MinimumSize = new System.Drawing.Size(845, 450);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "cFilters";
            ((System.ComponentModel.ISupportInitialize)(this.pbOriginal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFilter)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbOriginal;
        private System.Windows.Forms.PictureBox pbFilter;
        private System.Windows.Forms.Label lbOriginal;
        private System.Windows.Forms.Label lbFilter;
        private System.Windows.Forms.Button btOpen;
        private System.Windows.Forms.OpenFileDialog ofdOriginal;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cbFilters;
        private System.Windows.Forms.Button btProcess;
    }
}

