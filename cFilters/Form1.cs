﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Drawing.Imaging;

namespace cFilters
{
    public partial class Form1 : Form
    {
        string strPath;
        Image imgInit;
        Bitmap bmpOri, bmpFil;
        short width, height;
        #region segmentacion
        List<obj> objects;
        int[,] mapObj;
        sPoint minObj;
        #endregion
        
        public Form1()
        {
            InitializeComponent();
            strPath = Directory.GetCurrentDirectory() + "\\";
            width = 640;
            height = 480;
            minObj = new sPoint(30, 30);
            cbFilters.SelectedIndex = 0;
        }

        private void btOpen_Click(object sender, EventArgs e)
        {
            ofdOriginal.InitialDirectory = strPath + "Examples";
            if (ofdOriginal.ShowDialog() == DialogResult.OK)
            {
                imgInit = Image.FromFile(ofdOriginal.FileName);
                bmpOri = new Bitmap(imgInit);
                imgInit.Dispose();
                if (bmpOri.Width == width && bmpOri.Height == height)
                {
                    pbOriginal.Image = bmpOri;
                }
                else
                {
                    pbOriginal.Image = null;
                    MessageBox.Show("La imagen debe ser de 640x480 pixeles");
                }             
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (pbFilter.Image != null)
            {
                pbFilter.Image.Save("filteredImg.bmp", ImageFormat.Bmp);
                MessageBox.Show("Imagen guardada");
            }
            else
            {
                MessageBox.Show("No hay imagen a guardar");
            }
        }

        private void btProcess_Click(object sender, EventArgs e)
        {
            if (pbOriginal.Image != null)
            {
                statusControls(false);
                byte[,,] matOri = bmp3mat((Bitmap)pbOriginal.Image);            
                switch(cbFilters.SelectedIndex)
                {
                    case 0: //Sin filtros
                        bmpFil = mat3bmp(matOri);
                        break;
                    case 1: //Invertir en horizonal
                        byte[,,] fih1 = turnHor(matOri);
                        bmpFil = mat3bmp(fih1);
                        break;
                    case 2: //Invertir en vertical
                        byte[,,] fiv1 = turnVer(matOri);
                        bmpFil = mat3bmp(fiv1);
                        break;
                    case 3: //Escala de grises
                        byte[,] fgs1 = grayScale(matOri);
                        bmpFil = mat1bmp(fgs1);
                        break;
                    case 4: //Binarizada
                        byte[,] fgs2 = grayScale(matOri);
                        byte[,] fb1 = binarized(fgs2);
                        bmpFil = mat1bmp(fb1);
                        break;
                    case 5: //Colores invertidos
                        byte[,,] fci1 = colorsInv(matOri);
                        bmpFil = mat3bmp(fci1);
                        break;
                    case 6: //8 colores
                        byte[,,] fec1 = eightBits(matOri);
                        bmpFil = mat3bmp(fec1);
                        break;
                    case 7: //Detección de bordes
                        byte[,] fgs3 = grayScale(matOri);
                        byte[,] fed1 = edgeDetect(fgs3);
                        bmpFil = mat1bmp(fed1);
                        break;
                    case 8: //Segmentación
                        byte[,] fgs4 = grayScale(matOri);
                        mapObj = segmented(fgs4);
                        if (objects.Count > 0)
                        {
                            string strObjs = strPath + "objects\\";
                            if (Directory.Exists(strObjs) == true)
                            {
                                Directory.Delete(strObjs, true);
                            }
                            Directory.CreateDirectory(strObjs);
                            while (Directory.Exists(strObjs) == false)
                            {
                                Thread.Sleep(10);
                            }
                            string[] infoObjs = new string[objects.Count];
                            for (short o = 0; o < objects.Count; o++)
                            {
                                infoObjs[o] = "Objeto " + objects[o].index + "  { Inicio:" +
                                    objects[o].pIni.x + "," + objects[o].pIni.y + "  Fin:" +
                                    objects[o].pEnd.x + "," + objects[o].pEnd.y + " } Width: " +
                                    objects[o].width + "  Height: " + objects[o].height +
                                    "  Area:" + objects[o].area;
                                Bitmap obmp = new Bitmap(objects[o].width, objects[o].height);
                                short auxX, auxY = 0;
                                for (short y = objects[o].pIni.y; y <= objects[o].pEnd.y; y++)
                                {
                                    auxX = 0;
                                    for (short x = objects[o].pIni.x; x <= objects[o].pEnd.x; x++)
                                    {
                                        if (mapObj[x, y] == objects[o].index)
                                        {
                                            obmp.SetPixel(auxX, auxY, Color.Lime);
                                        }
                                        auxX++;
                                    }
                                    auxY++;
                                }
                                obmp.Save(strObjs + "obj" + objects[o].index + ".bmp", ImageFormat.Bmp);
                            }
                            File.WriteAllLines(strObjs + "info.txt", infoObjs);
                            MessageBox.Show("Segmentos guardados");
                        }
                        else
                        {
                            MessageBox.Show("Sin segmentos");
                        }
                        bmpFil = new Bitmap(width, height);
                        break;
                }
                pbFilter.Image = bmpFil;
                statusControls(true);
            }
            else
            {
                MessageBox.Show("No hay imagen cargada");
            }
        }

        private void statusControls(bool st)
        {
            if (st == true)
            {
                btProcess.Text = "Procesar";
                this.Cursor = Cursors.Default;
            }
            else
            {
                btProcess.Text = "Procesando";
                this.Cursor = Cursors.AppStarting;
            }
            btProcess.Enabled = st;
            cbFilters.Enabled = st;
            btOpen.Enabled = st;
            btnSave.Enabled = st;
        }

        //###############################  CONVERTIDORES ###############################

        private byte[,,] bmp3mat(Bitmap bmp)
        {
            byte[,,] mat = new byte[width, height, 3];
            for (short y = 0; y < height; y++)
            {
                for(short x = 0; x < width; x++)
                {
                    mat[x, y, 0] = (byte)bmp.GetPixel(x, y).R;
                    mat[x, y, 1] = (byte)bmp.GetPixel(x, y).G;
                    mat[x, y, 2] = (byte)bmp.GetPixel(x, y).B;
                }
            }
            return mat;
        }

        private Bitmap mat3bmp(byte[,,] mat)
        {
            Bitmap bmp = new Bitmap(width, height);
            Color clr = new Color();
            for (short y = 0; y < height; y++)
            {
                for (short x = 0; x < width; x++)
                {
                    clr = Color.FromArgb(mat[x, y, 0], mat[x, y, 1], mat[x, y, 2]);
                    bmp.SetPixel(x, y, clr);
                }
            }
            return bmp;
        }

        private Bitmap mat1bmp(byte[,] mat)
        {
            Bitmap bmp = new Bitmap(width, height);
            Color clr = new Color();
            for (short y = 0; y < height; y++)
            {
                for (short x = 0; x < width; x++)
                {
                    clr = Color.FromArgb(mat[x, y], mat[x, y], mat[x, y]);
                    bmp.SetPixel(x, y, clr);
                }
            }
            return bmp;
        }

        //###############################  FILTROS ###############################

        private byte[,] grayScale(byte[,,] rgb)
        {
            byte[,] gs = new byte[width, height];
            int aux;
            for (short y = 0; y < height; y++)
            {
                for (short x = 0; x < width; x++)
                {
                    aux = rgb[x, y, 0] + rgb[x, y, 1] + rgb[x, y, 2];
                    gs[x, y] = (byte)(aux / 3);
                }
            }
            return gs;
        }

        private byte[,] binarized(byte[,] gs)
        {
            byte[,] bin = new byte[width, height];
            for (short y = 0; y < height; y++)
            {
                for (short x = 0; x < width; x++)
                {
                    if (gs[x, y] < 127)
                    {
                        bin[x, y] = 0;
                    }
                    else
                    {
                        bin[x, y] = 255;
                    }
                }
            }
            return bin;
        }

        private byte[,,] colorsInv(byte[,,] rgb)
        {
            byte[,,] inv = new byte[width, height, 3];
            for (short y = 0; y < height; y++)
            {
                for (short x = 0; x < width; x++)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        inv[x, y, i] = (byte)(255 - rgb[x, y, i]);
                    }
                }
            }
            return inv;
        }

        private byte[,,] eightBits(byte[,,] rgb)
        {
            byte[,,] eight = new byte[width, height, 3];
            for (short y = 0; y < height; y++)
            {
                for (short x = 0; x < width; x++)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        if (rgb[x, y, i] < 127)
                        {
                            eight[x, y, i] = 0;
                        }
                        else
                        {
                            eight[x, y, i] = 255;
                        }
                    }  
                }
            }
            return eight;
        }

        private byte[,] edgeDetect(byte[,] gs)
        {
            byte[,] ed = new byte[width, height];
            int[,] gx = { { -1, 0, 1 }, { -2, 0, 2 }, { -1, 0, 1 } };
            int[,] gy = { { 1, 2, 1 }, { 0, 0, 0 }, { -1, -2, -1 } };
            int nx, ny, pc;
            for (short y = 1; y < height - 1; y++)
            {
                for (short x = 1; x < width - 1; x++)
                {
                    nx = ny = 0;
                    for (short yy = -1; yy < 2; yy++)
                    {
                        for (short xx = -1; xx < 2; xx++)
                        {
                            pc = gs[x + xx, y + yy];
                            nx += gx[xx + 1, yy + 1] * pc;
                            ny += gy[xx + 1, yy + 1] * pc;
                        }
                    }
                    if ((nx * nx + ny * ny) > 10000)
                    {
                        ed[x, y] = 0;
                    }
                    else
                    {
                        ed[x, y] = 255;
                    }
                }
            }
            return ed;
        }

        private byte[,,] turnHor(byte[,,] rgb)
        {
            byte[,,] turnx = new byte[width, height, 3];
            int auxX;
            for (short y = 0; y < height; y++)
            {
                auxX = width - 1;
                for (short x = 0; x < width; x++)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        turnx[x, y, i] = rgb[auxX, y, i];
                    }
                    auxX--;
                }
            }
            return turnx;
        }

        private byte[,,] turnVer(byte[,,] rgb)
        {
            byte[,,] turnx = new byte[width, height, 3];
            int auxY = height - 1; ;
            for (short y = 0; y < height; y++)
            {
                for (short x = 0; x < width; x++)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        turnx[x, y, i] = rgb[x, auxY, i];
                    }
                }
                auxY--;
            }
            return turnx;
        }

        private int[,] segmented(byte[,] gs)
        {
            int[,] map = new int[width, height];
            for (short y = 1; y < height - 1; y++)
            {
                for (short x = 1; x < width - 1; x++)
                {
                    if (gs[x, y] < 127)
                    {
                        map[x, y] = -1;
                    }
                    else
                    {
                        map[x, y] = 0;
                    }
                }
            }
            for (short i = 0; i < width; i++)
            {
                map[i, 0] = 0;
                map[i, height - 1] = 0;
            }
            for (short i = 0; i < height; i++)
            {
                map[0, i] = 0;
                map[width - 1, i] = 0;
            }
            int index = 0, selIndex, selX, selY, minX, minY, maxX, maxY, area;
            List<sPoint> tmp;
            objects = new List<obj>();
            for (short y = 1; y < height - 1; y++)
            {
                for (short x = 1; x < width - 1; x++)
                {
                    if (map[x, y] == -1)
                    {
                        index++;
                        tmp = new List<sPoint>();
                        tmp.Add(new sPoint(x, y));
                        minX = maxX = x;
                        minY = maxY = y;
                        area = 0;
                        while (tmp.Count > 0)
                        {
                            selIndex = tmp.Count - 1;
                            selX = tmp[selIndex].x;
                            selY = tmp[selIndex].y;
                            map[selX, selY] = index;
                            area++;
                            for (short yy = (short)(selY - 1); yy < selY + 2; yy++)
                            {
                                for (short xx = (short)(selX - 1); xx < selX + 2; xx++)
                                {
                                    if (map[xx, yy] == -1)
                                    {
                                        tmp.Add(new sPoint(xx, yy));
                                    }
                                }
                            } 
                            if (selX < minX)
                            {
                                minX = selX;
                            }
                            if (selX > maxX)
                            {
                                maxX = selX;
                            }
                            if (selY < minY)
                            {
                                minY = selY;
                            }
                            if (selY > maxY)
                            {
                                maxY = selY;
                            }
                            tmp.RemoveAt(selIndex); 
                        }
                        if ((maxX - minX + 1) >= minObj.x && (maxY - minY + 1) >= minObj.y)
                        {
                            objects.Add(new obj((short)minX, (short)minY, (short)maxX, (short)maxY, index, area));
                        }
                    }
                }
            }
            return map;
        }

    }
}
