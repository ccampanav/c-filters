﻿namespace cFilters
{
    class obj
    {
        public sPoint pIni, pEnd;
        public short width, height;
        public int index, area;

        public obj(short x1, short y1, short x2, short y2, int i, int a)
        {
            pIni = new sPoint(x1, y1);
            pEnd = new sPoint(x2, y2);
            width = (short)(pEnd.x - pIni.x + 1);
            height = (short)(pEnd.y - pIni.y + 1);
            index = i;
            area = a;
        }
    }
}
